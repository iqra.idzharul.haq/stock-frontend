import React, { Component, Fragment } from 'react';
import { Container, Card, CardBody, Row, Col, Table, CardHeader, Label } from 'reactstrap';
import DashboardCard from '../compnents/DashboardCard';

class DashboardHome extends Component<any, any> {
  render() {
    
    return (
      <Container>
        <Row>
          <Col>
            <DashboardCard title="TOTAL USERS" value="137" icon="fa fa-user-circle fa-2x align-self-flex-end" color="white" fontColor="black"/>
          </Col>
          <Col>
            <DashboardCard title="TOTAL PRODUCTS" value="24" icon="fa fa-archive fa-2x align-self-flex-end" color="white" fontColor="black"/>
          </Col>
          <Col>
            <DashboardCard title="TOTAL ORDERS" value="37" icon="fa fa-pencil fa-2x align-self-flex-end" color="white" fontColor="black"/>
          </Col>
          <Col>
            <DashboardCard title="TOTAL INCOME" value="$ 1,350" icon="fa fa-dollar fa-2x align-self-flex-end" color="#3f51b5" fontColor="white"/>
          </Col>
        </Row>
        <Row>
          <Col lg="4" md="4">
            <Card>
              <CardHeader>
                Latest Products
              </CardHeader>
              <CardBody>
                <Fragment>
                  <Row className="mar-bot-10">
                    <Col md="4" lg="4" className="text-align-center">
                      <i className="fa fa-camera fa-2x"></i>
                    </Col>
                    <Col md="6" lg="6">
                      <h6> Product 1</h6>
                      <Label>Details</Label>
                    </Col>
                    <Col md="2" lg="2" style={{color: "red"}}>
                      <i className="fa fa-trash"></i>
                    </Col>
                  </Row>
                </Fragment>
                <Fragment>
                  <Row className="mar-bot-10">
                    <Col md="4" lg="4" className="text-align-center">
                      <i className="fa fa-archive fa-2x"></i>
                    </Col>
                    <Col md="6" lg="6">
                      <h6> Product 2</h6>
                      <Label>Details</Label>
                    </Col>
                    <Col md="2" lg="2" style={{color: "red"}}>
                      <i className="fa fa-trash"></i>
                    </Col>
                  </Row>
                </Fragment>
                <Fragment>
                  <Row className="mar-bot-10">
                    <Col md="4" lg="4" className="text-align-center">
                      <i className="fa fa-balance-scale fa-2x"></i>
                    </Col>
                    <Col md="6" lg="6">
                      <h6> Product 3</h6>
                      <Label>Details</Label>
                    </Col>
                    <Col md="2" lg="2" style={{color: "red"}}>
                      <i className="fa fa-trash"></i>
                    </Col>
                  </Row>
                </Fragment>
                <Label  className="text-align-right">
                  View All &nbsp;&nbsp; <i className="fa fa-caret-right"/>
                </Label>
              </CardBody>
            </Card>
          </Col>
          <Col lg="8" md="8">
            <Card>
              <CardHeader>
                Latest Orders
              </CardHeader>
              <Table borderless hover size="sm">
                <thead>
                  <tr>
                    <th className="th-center-middle min-width-100 text-align-center">Transaction</th>
                    <th className="th-center-middle min-width-100 text-align-center" >Date</th>
                    <th className="th-center-middle min-width-100 text-align-center" >Type</th>
                    <th className="th-center-middle min-width-100 text-align-center" >Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="tableodd">
                    <td className="text-align-center">83Mfjertmnshfk</td>
                    <td className="text-align-center">28/11/2019</td>
                    <td className="text-align-center">Cash out</td>
                    <td className="text-align-center">DONE</td>
                  </tr>
                  <tr className="tableodd">
                    <td className="text-align-center">83Mfjertmnshfk</td>
                    <td className="text-align-center">28/11/2019</td>
                    <td className="text-align-center">Cash in</td>
                    <td className="text-align-center">DONE</td>
                  </tr>
                  <tr className="tableodd">
                    <td className="text-align-center">83Mfjertmnshfk</td>
                    <td className="text-align-center">28/11/2019</td>
                    <td className="text-align-center">Cash Out</td>
                    <td className="text-align-center">DONE</td>
                  </tr>
                  <tr className="tableodd">
                    <td className="text-align-center">83Mfjertmnshfk</td>
                    <td className="text-align-center">28/11/2019</td>
                    <td className="text-align-center">Cash in</td>
                    <td className="text-align-center">DONE</td>
                  </tr>
                </tbody>
              </Table>
              <div className="text-align-right mar-bot-15 pad-3-13">
                View All &nbsp;&nbsp; <i className="fa fa-caret-right"/>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default DashboardHome;
