import React from 'react';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  {
    path: '/company',
    exact: true,
    name: 'Company',
    component: React.lazy(() => import('./views/CompanySummary')),
  },
];

export default routes;
