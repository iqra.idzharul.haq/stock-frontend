import React, { Component, Fragment } from 'react';
import { Table, Card, Col, Row, Form, Label, Input, Button } from 'reactstrap';
import axios from 'axios'

class CompanySummary extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      dataAPI: {},
      filter: {
        companyName: '',
        address: '',
        sector: '',
        date1: '',
        date2: '',
      },
    };
  }

  async componentDidMount(){
    const response = await axios.get('http://localhost/slim/data/branch/ID');
    this.setState({dataAPI: response.data})
  }
  
  onChange = (e: any) =>{
    const { filter } = this.state;
    const copyFilter = filter;
    copyFilter[e.target.name] = e.target.value;
    this.setState({filter:copyFilter})
  }
  onSearch = () => {
    const { filter, dataAPI } = this.state;
    console.log(dataAPI)
  }

  render() { 
    const { filter } = this.state;

    return (
        <div className = "animated fadeIn">
         
        <Fragment>
          <Col>
            <Form>
              <Row className="mar-bot-5">
                <Col md='2' lg='2'>
                  <Label>Company Name</Label>
                </Col>
                <Col md='3' lg='3'> 
                  <Input name="companyName" value={filter.companyName} onChange={(e) => {this.onChange(e)}}/>
                </Col>
              </Row>
              <Row className="mar-bot-5">
                <Col md='2' lg='2'>
                  <Label>Address</Label>
                </Col>
                <Col md='3' lg='3'> 
                  <Input name="address" value={filter.address} onChange={(e) => {this.onChange(e)}}/>
                </Col>
              </Row>
              <Row className="mar-bot-5">
                <Col md='2' lg='2'>
                  <Label>Sector</Label>
                </Col>
                <Col md='3' lg='3'> 
                  <Input type='select' name="sector" value={filter.sector} onChange={(e) => {this.onChange(e)}}>
                    <option value='1'>Pertanian</option>
                    <option value='2'>Makanan</option>
                  </Input>
                </Col>
              </Row>
              <Row className="mar-bot-5">
                <Col md='2' lg='2'>
                  <Label>Date1</Label>
                </Col>
                <Col md='3' lg='3'> 
                  <Input type='date' name="date1" value={filter.date1} onChange={(e) => {this.onChange(e)}}/>
                </Col>
              </Row>
              <Row className="mar-bot-5">
                <Col md='2' lg='2'>
                  <Label>date2</Label>
                </Col>
                <Col md='3' lg='3'> 
                  <Input type='date' name="date2" value={filter.date2} onChange={(e) => {this.onChange(e)}}/>
                </Col>
              </Row>
              <Row className="mar-bot-5">
                <Col>
                <Button color="primary" onClick={() => {this.onSearch()}}>
                search 
              </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Fragment>


          <Card>
            <Table hover borderless responsive>
              <thead>
                <th className = "th-center-middle min-width-140"> Company Name</th> 
                <th> Address</th> 
                <th> Sector</th> 
                <th> Date1</th> 
                <th> Date2</th> 
              </thead>

              <tbody>
                <tr>
                  <td>PT Makmur</td>
                  <td>Babakan</td>
                  <td>Pertanian</td>
                  <td>17 Mei</td>
                  <td>18 Mei</td>
                </tr>
                <tr>
                  <td>PT Jaya</td>
                  <td>Bibikin</td>
                  <td>Makanan</td>
                  <td>19 Mei</td>
                  <td>20 Mei</td>
                </tr>

              </tbody>
            </Table>
          </Card>
      </div>
    )
  }
}

export default CompanySummary;