import React, { Component } from 'react';
import { Container } from 'reactstrap';

class Avatar extends Component<any, any> {
  render() {
    
    return (
      <Container className="text-align-center mar-top-15">
        <img src="../../assets/img/avatars/3.jpg" className="img-avatar mar-bot-10" style={{width:60}} alt="avatar"/>
        <h5>Alexandra Tim</h5>
        <h6>Administrator</h6>
      </Container>
    );
  }
}

export default Avatar;
