import React from 'react';

export const CompanyName = 'Company Name';
export const ProjectName = 'Project Name';

export const DefaultLayout = React.lazy(() => import('../components/containers/DefaultLayout'));
